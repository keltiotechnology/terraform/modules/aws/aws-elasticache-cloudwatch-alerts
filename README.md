<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
# Cloudwatch alerts for Amazon ElastiCache

## Usage

By default, all alerts are disabled, but we can also explicitly set monitor\_<alert\_name> to false:
```hcl-terraform
module "cloud_watch_elastic_cache" {
  source                    = "git::https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-elasticache-cloudwatch-alerts"
  namespace                 = "devops"
  stage                     = "dev"

  member_clusters           = ["ID of Elastic Cache member clusters"]
# if you use cloudposse elastic cache module,
# member_clusters           = flatten(module.<cloudposse redis module>.member_clusters)

  monitor_cpu_utilization   = true
  monitor_freeable_memory   = true
  monitor_swap_usage        = true
  monitor_evictions         = true
  monitor_curr_connections  = true
}
```

## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudwatch_metric_alarm.cpu_utilization](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.curr_connections](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.evictions](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.freeable_memory](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |
| [aws_cloudwatch_metric_alarm.swap_usage](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudwatch_metric_alarm) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_cpu_utilization"></a> [cpu\_utilization](#input\_cpu\_utilization) | Configure alert when the ElastiCache CPU Utilization (Percent) is greater than or equal to threshold | <pre>object({<br>    alarm_name         = string<br>    evaluation_periods = number<br>    period             = number<br>    threshold          = number<br>    alarm_actions      = list(string)<br>  })</pre> | <pre>{<br>  "alarm_actions": [],<br>  "alarm_name": "ElastiCache-CPU-Utilization",<br>  "evaluation_periods": 1,<br>  "period": 300,<br>  "threshold": 80<br>}</pre> | no |
| <a name="input_curr_connections"></a> [curr\_connections](#input\_curr\_connections) | Configure alert when the ElastiCache's CurrConnections is greater than or equal to a specified value | <pre>object({<br>    alarm_name         = string<br>    evaluation_periods = number<br>    period             = number<br>    threshold          = number<br>    alarm_actions      = list(string)<br>  })</pre> | <pre>{<br>  "alarm_actions": [],<br>  "alarm_name": "ElastiCache-Curr-Connections",<br>  "evaluation_periods": 1,<br>  "period": 300,<br>  "threshold": 100<br>}</pre> | no |
| <a name="input_evictions"></a> [evictions](#input\_evictions) | Configure alert when the ElastiCache's Evictions is greater than or equal to a specified value | <pre>object({<br>    alarm_name         = string<br>    evaluation_periods = number<br>    period             = number<br>    threshold          = number<br>    alarm_actions      = list(string)<br>  })</pre> | <pre>{<br>  "alarm_actions": [],<br>  "alarm_name": "ElastiCache-Evictions",<br>  "evaluation_periods": 1,<br>  "period": 300,<br>  "threshold": 1<br>}</pre> | no |
| <a name="input_freeable_memory"></a> [freeable\_memory](#input\_freeable\_memory) | Configure alert when the ElastiCache's FreeableMemory is below or equal to a specified value | <pre>object({<br>    alarm_name         = string<br>    evaluation_periods = number<br>    period             = number<br>    threshold          = number<br>    alarm_actions      = list(string)<br>  })</pre> | <pre>{<br>  "alarm_actions": [],<br>  "alarm_name": "ElastiCache-Freeable-Memory",<br>  "evaluation_periods": 1,<br>  "period": 300,<br>  "threshold": 100000<br>}</pre> | no |
| <a name="input_member_clusters"></a> [member\_clusters](#input\_member\_clusters) | IDs of Elasti Cache member clusters | `list(string)` | n/a | yes |
| <a name="input_monitor_cpu_utilization"></a> [monitor\_cpu\_utilization](#input\_monitor\_cpu\_utilization) | Set true to enable monitoring ElastiCache CPU Utilization | `bool` | `false` | no |
| <a name="input_monitor_curr_connections"></a> [monitor\_curr\_connections](#input\_monitor\_curr\_connections) | Set true to enable monitoring CurrConnections | `bool` | `false` | no |
| <a name="input_monitor_evictions"></a> [monitor\_evictions](#input\_monitor\_evictions) | Set true to enable monitoring Evictions | `bool` | `false` | no |
| <a name="input_monitor_freeable_memory"></a> [monitor\_freeable\_memory](#input\_monitor\_freeable\_memory) | Set true to enable monitoring ElastiCache Freeable memory | `bool` | `false` | no |
| <a name="input_monitor_swap_usage"></a> [monitor\_swap\_usage](#input\_monitor\_swap\_usage) | Set true to enable monitoring SwapUsage | `bool` | `false` | no |
| <a name="input_namespace"></a> [namespace](#input\_namespace) | Namespace, which could be your organization name | `string` | `""` | no |
| <a name="input_stage"></a> [stage](#input\_stage) | Stage, which can be 'prod', 'staging', 'dev'... | `string` | `""` | no |
| <a name="input_swap_usage"></a> [swap\_usage](#input\_swap\_usage) | Configure alert when the ElastiCache's SwapUsage is greater than or equal to a specified value | <pre>object({<br>    alarm_name         = string<br>    evaluation_periods = number<br>    period             = number<br>    threshold          = number<br>    alarm_actions      = list(string)<br>  })</pre> | <pre>{<br>  "alarm_actions": [],<br>  "alarm_name": "ElastiCache-Swap-usage",<br>  "evaluation_periods": 1,<br>  "period": 300,<br>  "threshold": 50000000<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_alarm_elastic_cache_cpu_utilization_arn"></a> [alarm\_elastic\_cache\_cpu\_utilization\_arn](#output\_alarm\_elastic\_cache\_cpu\_utilization\_arn) | ARN of the cloudwatch metric alarm when the ElastiCache's CPU Utilization is greater than or equal to threshold |
| <a name="output_alarm_elastic_cache_evictions_arn"></a> [alarm\_elastic\_cache\_evictions\_arn](#output\_alarm\_elastic\_cache\_evictions\_arn) | ARN of the cloudwatch metric alarm when the ElastiCache's Evictions is greater than or equal to a specified value |
| <a name="output_alarm_elastic_cache_freeable_memory_arn"></a> [alarm\_elastic\_cache\_freeable\_memory\_arn](#output\_alarm\_elastic\_cache\_freeable\_memory\_arn) | ARN of the cloudwatch metric alarm when the ElastiCache's FreeableMemory is below or equal to a specified value |
| <a name="output_alarm_elastic_cache_swap_usage_arn"></a> [alarm\_elastic\_cache\_swap\_usage\_arn](#output\_alarm\_elastic\_cache\_swap\_usage\_arn) | ARN of the cloudwatch metric alarm when the ElastiCache's SwapUsage is greater than or equal to a specified value |
| <a name="output_alarm_elastic_curr_connections_arn"></a> [alarm\_elastic\_curr\_connections\_arn](#output\_alarm\_elastic\_curr\_connections\_arn) | ARN of the cloudwatch metric alarm when the ElastiCache's CurrConnections is greater than or equal to a specified value |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
