/* ------------------------------------------------------------------------------------------------------------ */
/* When CPU is close to max for a long period (CPUUtilization)                                                  */
/* ------------------------------------------------------------------------------------------------------------ */

resource "aws_cloudwatch_metric_alarm" "cpu_utilization" {
  for_each            = var.monitor_cpu_utilization ? toset(var.elasticache_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.cpu_utilization.alarm_name}-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.cpu_utilization.evaluation_periods
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ElastiCache"
  period              = var.cpu_utilization.period
  statistic           = "Average"
  threshold           = var.cpu_utilization.threshold
  alarm_description   = "Alert when the ElastiCache's CPU Utilization is greater than or equal to threshold"
  alarm_actions       = var.cpu_utilization.alarm_actions

  dimensions = {
    CacheClusterId = each.key
  }
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When FreeableMemory is below or equal to a specified value                                                   */
/* ------------------------------------------------------------------------------------------------------------ */

resource "aws_cloudwatch_metric_alarm" "freeable_memory" {
  for_each            = var.monitor_freeable_memory ? toset(var.elasticache_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.freeable_memory.alarm_name}-${each.key}"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = var.freeable_memory.evaluation_periods
  metric_name         = "FreeableMemory"
  namespace           = "AWS/ElastiCache"
  period              = var.freeable_memory.period
  statistic           = "Average"
  threshold           = var.freeable_memory.threshold
  alarm_description   = "Alert when the ElastiCache's FreeableMemory is below or equal to a specified value"
  alarm_actions       = var.freeable_memory.alarm_actions

  dimensions = {
    CacheClusterId = each.key
  }
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When swap usage is close to a specific value (50 MB recommended threshold)                                   */
/* ------------------------------------------------------------------------------------------------------------ */

resource "aws_cloudwatch_metric_alarm" "swap_usage" {
  for_each               = var.monitor_swap_usage ? toset(var.elasticache_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.swap_usage.alarm_name}-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.swap_usage.evaluation_periods
  metric_name         = "SwapUsage"
  namespace           = "AWS/ElastiCache"
  period              = var.swap_usage.period
  statistic           = "Average"
  threshold           = var.swap_usage.threshold
  alarm_description   = "Alert when the ElastiCache's SwapUsage is greater than or equal to a specified value"
  alarm_actions       = var.swap_usage.alarm_actions

  dimensions = {
    CacheClusterId = each.key
  }
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When eviction metrics is above a value for a long period                                                     */
/* ------------------------------------------------------------------------------------------------------------ */

resource "aws_cloudwatch_metric_alarm" "evictions" {
  for_each               = var.monitor_evictions ? toset(var.elasticache_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.evictions.alarm_name}-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.evictions.evaluation_periods
  metric_name         = "Evictions"
  namespace           = "AWS/ElastiCache"
  period              = var.evictions.period
  statistic           = "Average"
  threshold           = var.evictions.threshold
  alarm_description   = "Alert when the ElastiCache's Evictions is greater than or equal to a specified value"
  alarm_actions       = var.evictions.alarm_actions

  dimensions = {
    CacheClusterId = each.key
  }
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When CurrConnections metrics is above a value for a long period                                              */
/* ------------------------------------------------------------------------------------------------------------ */

resource "aws_cloudwatch_metric_alarm" "curr_connections" {
  for_each               = var.monitor_curr_connections ? toset(var.elasticache_instances_ids) : []
  alarm_name          = "${var.namespace}/${var.stage}/${var.curr_connections.alarm_name}-${each.key}"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = var.curr_connections.evaluation_periods
  metric_name         = "CurrConnections"
  namespace           = "AWS/ElastiCache"
  period              = var.curr_connections.period
  statistic           = "Average"
  threshold           = var.curr_connections.threshold
  alarm_description   = "Alert when the ElastiCache's CurrConnections is greater than or equal to a specified value"
  alarm_actions       = var.curr_connections.alarm_actions

  dimensions = {
    CacheClusterId = each.key
  }
}
