/* ------------------------------------------------------------------------------------------------------------ */
/* Outputs for Elasticache Alerts Module                                                                                */
/* ------------------------------------------------------------------------------------------------------------ */

output "alarm_elastic_cache_cpu_utilization_arn" {
  description = "ARNs of the CloudWatch CPU Utilization alarms for ElastiCache"
  value       = var.monitor_cpu_utilization ? [
    for alarm in aws_cloudwatch_metric_alarm.cpu_utilization : alarm.arn
  ] : []
}

output "alarm_elastic_cache_freeable_memory_arn" {
  description = "ARNs of the CloudWatch Freeable Memory alarms for ElastiCache"
  value       = var.monitor_freeable_memory ? [
    for alarm in aws_cloudwatch_metric_alarm.freeable_memory : alarm.arn
  ] : []
}

output "alarm_elastic_cache_swap_usage_arn" {
  description = "ARNs of the CloudWatch Swap Usage alarms for ElastiCache"
  value       = var.monitor_swap_usage ? [
    for alarm in aws_cloudwatch_metric_alarm.swap_usage : alarm.arn
  ] : []
}

output "alarm_elastic_cache_evictions_arn" {
  description = "ARNs of the CloudWatch Evictions alarms for ElastiCache"
  value       = var.monitor_evictions ? [
    for alarm in aws_cloudwatch_metric_alarm.evictions : alarm.arn
  ] : []
}

output "alarm_elastic_cache_curr_connections_arn" {
  description = "ARNs of the CloudWatch Current Connections alarms for ElastiCache"
  value       = var.monitor_curr_connections ? [
    for alarm in aws_cloudwatch_metric_alarm.curr_connections : alarm.arn
  ] : []
}