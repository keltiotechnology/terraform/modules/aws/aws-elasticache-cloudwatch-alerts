/* ------------------------------------------------------------------------------------------------------------ */
/* Context variables                                                                                             */
/* ------------------------------------------------------------------------------------------------------------ */
variable "namespace" {
  type        = string
  description = "Namespace, which could be your organization name"
  default     = ""
}

variable "stage" {
  type        = string
  description = "Stage, which can be 'prod', 'staging', 'dev'..."
  default     = ""
}

variable "elasticache_instances_ids" {
  type        = list(string)
  description = "IDs of Elasti Cache member instances"
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When CPU is close to max for a long period (CPUUtilization)                                                  */
/* ------------------------------------------------------------------------------------------------------------ */
variable "cpu_utilization" {
  description = "Configure alert when the ElastiCache CPU Utilization (Percent) is greater than or equal to threshold"

  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })

  default = {
    alarm_name         = "ElastiCache-CPU-Utilization"
    evaluation_periods = 1
    period             = 300
    threshold          = 80
    alarm_actions      = []
  }
}

variable "monitor_cpu_utilization" {
  type        = bool
  description = "Set true to enable monitoring ElastiCache CPU Utilization"
  default     = false
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When FreeableMemory is below or equal to a specified value                                                   */
/* ------------------------------------------------------------------------------------------------------------ */
variable "freeable_memory" {
  description = "Configure alert when the ElastiCache's FreeableMemory is below or equal to a specified value"

  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })

  default = {
    alarm_name         = "ElastiCache-Freeable-Memory"
    evaluation_periods = 1
    period             = 300
    threshold          = 100000
    alarm_actions      = []
  }
}

variable "monitor_freeable_memory" {
  type        = bool
  description = "Set true to enable monitoring ElastiCache Freeable memory"
  default     = false
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When swap usage is close to a specific value (50 MB recommended threshold)                                   */
/* ------------------------------------------------------------------------------------------------------------ */
variable "swap_usage" {
  description = "Configure alert when the ElastiCache's SwapUsage is greater than or equal to a specified value"

  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })

  default = {
    alarm_name         = "ElastiCache-Swap-usage"
    evaluation_periods = 1
    period             = 300
    threshold          = 50000000 # 50 MB
    alarm_actions      = []
  }
}

variable "monitor_swap_usage" {
  type        = bool
  description = "Set true to enable monitoring SwapUsage"
  default     = false
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When eviction metrics is above a value for a long period                                                     */
/* ------------------------------------------------------------------------------------------------------------ */
variable "evictions" {
  description = "Configure alert when the ElastiCache's Evictions is greater than or equal to a specified value"

  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })

  default = {
    alarm_name         = "ElastiCache-Evictions"
    evaluation_periods = 1
    period             = 300
    threshold          = 1
    alarm_actions      = []
  }
}

variable "monitor_evictions" {
  type        = bool
  description = "Set true to enable monitoring Evictions"
  default     = false
}


/* ------------------------------------------------------------------------------------------------------------ */
/* When CurrConnections metrics is above a value for a long period                                              */
/* ------------------------------------------------------------------------------------------------------------ */
variable "curr_connections" {
  description = "Configure alert when the ElastiCache's CurrConnections is greater than or equal to a specified value"

  type = object({
    alarm_name         = string
    evaluation_periods = number
    period             = number
    threshold          = number
    alarm_actions      = list(string)
  })

  default = {
    alarm_name         = "ElastiCache-Curr-Connections"
    evaluation_periods = 1
    period             = 300
    threshold          = 100
    alarm_actions      = []
  }
}

variable "monitor_curr_connections" {
  type        = bool
  description = "Set true to enable monitoring CurrConnections"
  default     = false
}